<?php

use LendInvest\Investor;
use LendInvest\Loan;
use LendInvest\Tranche;
use PHPUnit\Framework\TestCase;

class AcceptanceTest extends TestCase
{
    /**
     * @test
     */
    public function firstScenario()
    {
        $loan = new Loan(new DateTime('10/01/2015'), new DateTime('11/15/2015'));
        $tranche = new Tranche($loan, 0.03, 1000);
        $investor = new Investor(1000);

        $tranche->invest($investor, 1000, new DateTime('10/03/2015'));
        $interest = $investor->getInterestForMonth(new DateTime('10/01/2015'));

        $this->assertEquals(28.06, $interest);
    }

    /**
     * @test
     * @expectedException Exception
     * @expectedExceptionMessage Sorry! The tranche has not enough funds
     */
    public function secondScenario()
    {
        $loan = new Loan(new DateTime('10/01/2015'), new DateTime('11/15/2015'));
        $tranche = new Tranche($loan, 0.03, 1000);
        $investorOne = new Investor(1000);
        $investorTwo = new Investor(1000);

        $tranche->invest($investorOne, 1000, new DateTime('10/03/2015'));
        $tranche->invest($investorTwo, 1, new DateTime('10/04/2015'));
    }

    /**
     * @test
     */
    public function thirdScenario()
    {
        $loan = new Loan(new DateTime('10/01/2015'), new DateTime('11/15/2015'));
        $trancheB = new Tranche($loan, 0.06, 1000);
        $investorThree = new Investor(1000);

        $trancheB->invest($investorThree, 500, new DateTime('10/10/2015'));
        $interest = $investorThree->getInterestForMonth(new DateTime('10/01/2015'));

        $this->assertEquals(21.29, $interest);
    }

    /**
     * @test
     * @expectedException Exception
     * @expectedExceptionMessage Sorry! The tranche has not enough funds
     */
    public function fourthScenario()
    {
        $loan = new Loan(new DateTime('10/01/2015'), new DateTime('11/15/2015'));
        $tranche = new Tranche($loan, 0.06, 1000);
        $investor = new Investor(1000);

        $tranche->invest($investor, 1100, new DateTime('10/25/2015'));
    }
}
