<?php

use LendInvest\Loan;
use PHPUnit\Framework\TestCase;

class LoanTest extends TestCase
{
    /**
     * @description Create loan successfully
     * @test
     */
    public function createLoan()
    {
        $startDate = new DateTime('2017-03-12');
        $endDate = new DateTime('2017-10-20');
        $loan = new Loan($startDate, $endDate);

        $this->assertEquals($startDate, $loan->getStartDate());
        $this->assertEquals($endDate, $loan->getEndDate());
    }

    /**
     * @description Create loan with start date after end date should throw exception
     * @expectedException Exception
     * @expectedExceptionMessage End date must be after start date
     * @test
     */
    public function wrongStartDate()
    {
        $endDate = new DateTime('2017-10-20');
        $startDate = new DateTime('2017-11-12');
        new Loan($startDate, $endDate);
    }

    /**
     * @description Create loan with end date before start date should throw exception
     * @expectedException Exception
     * @expectedExceptionMessage End date must be after start date
     * @test
     */
    public function wrongEndDate()
    {
        $endDate = new DateTime('2017-02-20');
        $startDate = new DateTime('2017-11-12');
        new Loan($startDate, $endDate);
    }
}
