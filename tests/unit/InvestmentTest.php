<?php

use LendInvest\Investment;
use LendInvest\Investor;
use LendInvest\Loan;
use LendInvest\Tranche;
use PHPUnit\Framework\TestCase;

class InvestmentTest extends TestCase
{
    /**
     * @description Creating investment successfully with mocked data
     * @test
     */
    public function createInvestment()
    {
        $date = new DateTime('2018-01-03');
        /** @var Investor $investor */
        $investor = $this->createMock(Investor::class);
        /** @var Tranche $tranche */
        $tranche = $this->createMock(Tranche::class);
        $loan = $this->createMock(Loan::class);
        $loan->expects($this->any())->method('isOpen')->willReturn(true);
        $tranche->expects($this->any())->method('getLoan')->willReturn($loan);
        $amount = 1000;
        $investment = new Investment($tranche, $investor, $date, $amount);

        $this->assertEquals($date, $investment->getDate());
        $this->assertEquals($investor, $investment->getInvestor());
        $this->assertEquals($amount, $investment->getAmount());
    }

    /**
     * @description Calculate investment monthly interest successfully
     * @test
     */
    public function calculateInterest()
    {
        $investment = $this->makeInvestment();
        $interest = $investment->calculateInterestForMonth(new DateTime('2018-01-01'));

        $this->assertTrue(is_float($interest));
        $this->assertEquals($interest, 28.06);
    }

    /**
     * @description Calculate investment interest for period before investment date
     * @test
     */
    public function differentRange()
    {
        $investment = $this->makeInvestment();
        $interest = $investment->calculateInterestForMonth(new DateTime('2017-12-01'));

        $this->assertEquals(0, $interest);
    }

    /**
     * @return Investment
     */
    private function makeInvestment() : Investment
    {
        $tranche = $this->createMock(Tranche::class);
        $loan = $this->createMock(Loan::class);
        $investor = $this->createMock(Investor::class);
        $loan->expects($this->any())->method('isOpen')->willReturn(true);
        $tranche->expects($this->any())->method('getLoan')->willReturn($loan);
        $tranche->expects($this->any())->method('getInterestRate')->willReturn(0.03);

        /** @var Tranche $tranche */
        /** @var Investor $investor */
        $investment = new Investment($tranche, $investor, new DateTime('2018-01-03'), 1000);

        return $investment;
    }
}
