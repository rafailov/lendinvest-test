<?php

use LendInvest\Wallet;
use PHPUnit\Framework\TestCase;

class WalletTest extends TestCase
{
    /**
     * @description Create wallet successfully
     * @test
     */
    public function createWallet()
    {
        $wallet = new Wallet(1000);

        $this->assertEquals(1000, $wallet->getFunds());
    }

    /**
     * @test
     */
    public function withdraw()
    {
        $wallet = new Wallet(1000);
        $wallet->withdraw(1000);

        $this->assertEquals(0, $wallet->getFunds());
    }

    /**
     * @test
     * @expectedException Exception
     * @expectedExceptionMessage You can not withdraw more than what you have
     */
    public function withdrawMore()
    {
        $wallet = new Wallet(1000);
        $wallet->withdraw(1100);
    }

    /**
     * @test
     * @expectedException Exception
     * @expectedExceptionMessage You can not fund your wallet with negative amount
     */
    public function fundNegative()
    {
        new Wallet(-10);
    }
}
