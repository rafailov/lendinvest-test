<?php

use LendInvest\Investment;
use LendInvest\Investor;
use LendInvest\Loan;
use LendInvest\Tranche;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class TrancheTest extends TestCase
{
    /**
     * @description Create tranche successfully
     * @test
     */
    public function createTranche()
    {
        $interestRate = 0.03;
        $maxInvest = 1000;
        /** @var Loan $loan */
        $loan = $this->createMock(Loan::class);
        $tranche = new Tranche($loan, $interestRate, $maxInvest);

        $this->assertEquals($interestRate, $tranche->getInterestRate());
        $this->assertEquals($maxInvest, $tranche->getMaxInvest());
        $this->assertEquals($loan, $tranche->getLoan());
    }

    /**
     * @test
     */
    public function calculateAvailableAmount()
    {
        /** @var Investment $investment */
        $investment = $this->makeInvestment(100);
        $loan = $this->createMock(Loan::class);
        $tranche = new Tranche($loan, 0.03, 1000);
        for ($i = 0; $i < 10; $i++) {
            $tranche->addInvestment($investment);
        }

        $this->assertEquals(0, $tranche->getAvailableAmount());
    }

    /**
     * @description Try to invest multiple investments in tranche with less availability
     * @expectedException Exception
     * @expectedExceptionMessage Sorry! The tranche has not enough funds
     * @test
     */
    public function InvestMoreThanAvailable()
    {
        /** @var Investment $investment */
        $investment = $this->makeInvestment(100);
        $loan = $this->createMock(Loan::class);
        $tranche = new Tranche($loan, 0.03, 1000);
        for ($i = 0; $i < 20; $i++) {
            $tranche->addInvestment($investment);
        }
    }

    /**
     * @description Try to invest investment with larger amount in tranche with less availability
     * @expectedException Exception
     * @expectedExceptionMessage Sorry! The tranche has not enough funds
     * @test
     */
    public function InvestBiggerThenAvailable()
    {
        /** @var Investment $investment */
        $investment = $this->makeInvestment(1100);
        $loan = $this->createMock(Loan::class);
        $tranche = new Tranche($loan, 0.03, 1000);
        $tranche->addInvestment($investment);
    }

    /**
     * @description Invest to tranche successfully
     * @test
     */
    public function investSuccessfully()
    {
        $tranche = $this->makeTranche();
        $investor = $this->createMock(Investor::class);
        /** @var Investor $investor */
        $tranche->invest($investor, 1000, new DateTime());

        $this->assertEquals(0, $tranche->getAvailableAmount());
    }

    /**
     * @expectedException Exception
     * @expectedExceptionMessage Sorry! The tranche has not enough funds
     * @test
     */
    public function investmentShouldFail()
    {
        $tranche = $this->makeTranche();
        $investor = $this->createMock(Investor::class);
        /** @var Investor $investor */
        $tranche->invest($investor, 1100, new DateTime());
    }

    /**
     * @param $amount
     * @return MockObject
     */
    private function makeInvestment($amount) : MockObject
    {
        $investment = $this->createMock(Investment::class);
        $investment->expects($this->any())->method('getAmount')->willReturn($amount);

        return $investment;
    }

    /**
     * @return Tranche
     */
    private function makeTranche(): Tranche
    {
        $loan = $this->createMock(Loan::class);
        $loan->expects($this->any())->method('isOpen')->willReturn(true);
        /** @var Loan $loan */
        $tranche = new Tranche($loan, 0.03, 1000);

        return $tranche;
    }
}
