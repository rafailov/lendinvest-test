<?php

use LendInvest\Investment;
use LendInvest\Investor;
use PHPUnit\Framework\TestCase;

class InvestorTest extends TestCase
{
    /**
     * @description Create investor and pay successfully
     * @test
     */
    public function createInvestor()
    {
        $funds = 100;
        $investor = new Investor($funds);

        $this->assertTrue($investor->hasEnoughFunds($funds));
        $investor->pay($funds);
        $this->assertFalse($investor->hasEnoughFunds($funds));
        $this->assertTrue($investor->hasEnoughFunds(0));
    }

    /**
     * @description Create investor and pay more than what he have should fail
     * @expectedException Exception
     * @expectedExceptionMessage You can not withdraw more than what you have
     * @test
     */
    public function paymentFailure()
    {
        $funds = 100;
        $investor = new Investor($funds);

        $investor->pay($funds + 1);
    }

    /**
     * @description Investor should be able to make investment
     * @test
     */
    public function addInvestment()
    {
        $investment = $this->createMock(Investment::class);
        $investment->expects($this->once())->method('getAmount')->willReturn(10);

        /** @var Investment $investment*/
        $investor = new Investor(20);
        $investor->makeInvestment($investment);
    }

    /**
     * @description Investor interest rate should be sum of all investments interest for this month
     * @test
     */
    public function calculateInterest()
    {
        $investment = $this->createMock(Investment::class);
        $investment->expects($this->any())->method('getAmount')->willReturn(10);
        $investment->expects($this->any())->method('calculateInterestForMonth')->willReturn(11);

        /** @var Investment $investment*/
        $investor = new Investor(30);
        $investor->makeInvestment($investment);
        $investor->makeInvestment($investment);
        $investor->makeInvestment($investment);

        $this->assertEquals(33, $investor->getInterestForMonth(new DateTime()));
    }

    /**
     * @description Investor try to invest too expensive investment should throw exception
     * @expectedException Exception
     * @expectedExceptionMessage You can not withdraw more than what you have
     * @test
     */
    public function fundsNotEnough()
    {
        $investment = $this->createMock(Investment::class);
        $investment->expects($this->once())->method('getAmount')->willReturn(100);

        /** @var Investment $investment*/
        $investor = new Investor(20);
        $investor->makeInvestment($investment);
    }
}
