<?php

use LendInvest\Investor;
use LendInvest\Loan;
use LendInvest\Tranche;

require_once 'bootstrap/app.php';

$loan = new Loan(new DateTime('10/01/2015'), new DateTime('11/15/2015'));

$trancheA = new Tranche($loan, 0.03, 1000);
$trancheB = new Tranche($loan, 0.06, 1000);

$investorOne = new Investor(1000);
$investorTwo = new Investor(1000);
$investorThree = new Investor(1000);
$investorFour = new Investor(1000);

$trancheA->invest($investorOne, 1000, new DateTime('10/03/2015'));
echo $investorOne->getInterestForMonth(new DateTime('10/01/2015')), "\n";

try {
    $trancheA->invest($investorTwo, 1, new DateTime('10/04/2015'));
} catch (Exception $exception) {
    echo $exception->getMessage(), "\n";
}

$trancheB->invest($investorThree, 500, new DateTime('10/10/2015'));
echo $investorThree->getInterestForMonth(new DateTime('10/01/2015')), "\n";

try {
    $trancheB->invest($investorFour, 1100, new DateTime('10/25/2015'));
} catch (Exception $exception) {
    echo $exception->getMessage(), "\n";
}
