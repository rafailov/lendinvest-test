<?php

require_once __DIR__ . '/../vendor/autoload.php';

set_exception_handler(function (Throwable $exception) {
    echo $exception->getMessage();
});
