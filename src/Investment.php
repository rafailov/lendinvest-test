<?php

namespace LendInvest;

use DateTime;
use Exception;

class Investment
{
    /**
     * @var Tranche
     */
    private $tranche;

    /**
     * @var Investor
     */
    private $investor;

    /**
     * @var int
     */
    private $amount;

    /**
     * @var DateTime
     */
    private $date;

    /**
     * Investment constructor.
     * @param Tranche $tranche
     * @param Investor $investor
     * @param DateTime $date
     * @param $amount
     * @throws Exception
     */
    public function __construct(Tranche $tranche, Investor $investor, DateTime $date, $amount)
    {
        if (! $tranche->getLoan() instanceof Loan) {
            throw new Exception('Sorry! this tranche is not part of a loan');
        }

        if (! $tranche->getLoan()->isOpen($date)) {
            throw new Exception('Sorry! loan is not open');
        }

        $this->tranche = $tranche;
        $this->investor = $investor;
        $this->amount = $amount;
        $this->date = $date;
    }

    /**
     * @return float
     */
    public function getAmount() : float
    {
        return $this->amount;
    }

    /**
     * @return DateTime
     */
    public function getDate() : DateTime
    {
        return $this->date;
    }

    /**
     * @return Tranche
     */
    public function getTranche() : Tranche
    {
        return $this->tranche;
    }

    /**
     * @return Investor
     */
    public function getInvestor() : Investor
    {
        return $this->investor;
    }

    /**
     * @param DateTime $date
     * @return float
     */
    public function calculateInterestForMonth(DateTime $date) : float
    {
        $numberOfDays = $date->format('t');
        $lastDayOfMonth = $date->modify('last day of this month');
        $numberOfInvestedDays = $this->getDate()->diff($lastDayOfMonth)->format('%r%a');
        if ($numberOfInvestedDays < 0) {
            return 0;
        }

        $accountableDays = $numberOfInvestedDays > $numberOfDays ? $numberOfDays : (int) $numberOfInvestedDays + 1;
        $interestRatePerDay = $this->getTranche()->getInterestRate() / $numberOfDays;
        $interestRateForPeriod = $accountableDays * $interestRatePerDay;

        return number_format($this->getAmount() * $interestRateForPeriod, 2);
    }
}
