<?php

namespace LendInvest;

use Exception;

class Wallet
{
    /**
     * @var int
     */
    private $wallerAmount;

    /**
     * Wallet constructor.
     * @param $amount
     */
    public function __construct($amount)
    {
        $this->fund($amount);
    }

    /**
     * @return float
     */
    public function getFunds() : float
    {
        return $this->wallerAmount;
    }

    /**
     * @param $amount
     * @return Wallet
     * @throws Exception
     */
    public function fund($amount) : Wallet
    {
        if ($amount < 0) {
            throw new Exception('You can not fund your wallet with negative amount');
        }

        $this->wallerAmount += $amount;

        return $this;
    }

    /**
     * @param $amount
     * @return Wallet
     * @throws Exception
     */
    public function withdraw($amount) : Wallet
    {
        if ($this->wallerAmount < $amount) {
            throw new Exception('You can not withdraw more than what you have');
        }

        $this->wallerAmount -= $amount;

        return $this;
    }
}
