<?php

namespace LendInvest;

use DateTime;
use Exception;

class Tranche
{
    /**
     * @var
     */
    private $interestRate;

    /**
     * @var float
     */
    private $maxInvest;

    /**
     * @var array
     */
    private $investments;

    /**
     * @var Loan
     */
    private $loan;

    /**
     * Tranche constructor.
     * @param Loan $loan
     * @param float $interestRate
     * @param int $maxInvest
     */
    public function __construct(Loan $loan, float $interestRate, int $maxInvest)
    {
        $this->loan = $loan;
        $this->interestRate = $interestRate;
        $this->maxInvest = $maxInvest;
        $this->investments = [];
    }

    /**
     * Calculate the available amount on each request for better accountability
     *
     * @return float
     */
    public function getAvailableAmount() : float
    {
        $result = $this->maxInvest;
        foreach ($this->investments as $investment) {
            $result -= $investment->getAmount();
        }

        return $result;
    }

    /**
     * @param Investor $investor
     * @param float $amount
     * @param DateTime|null $date
     * @return Tranche
     * @throws Exception
     */
    public function invest(Investor $investor, float $amount, DateTime $date) : Tranche
    {
        $investment = new Investment($this, $investor, $date, $amount);
        $this->addInvestment($investment);
        $investor->makeInvestment($investment);

        return $this;
    }

    /**
     * @return float
     */
    public function getInterestRate() : float
    {
        return $this->interestRate;
    }

    /**
     * @return int
     */
    public function getMaxInvest() : int
    {
        return $this->maxInvest;
    }

    /**
     * @return array
     */
    public function getInvestments() : array
    {
        return $this->investments;
    }

    /**
     * @param Investment $investment
     * @return Tranche
     * @throws Exception
     */
    public function addInvestment(Investment $investment) : Tranche
    {
        if ($this->getAvailableAmount() < $investment->getAmount()) {
            throw new Exception('Sorry! The tranche has not enough funds');
        }

        $this->investments[] = $investment;

        return $this;
    }

    /**
     * @return Loan
     */
    public function getLoan() : Loan
    {
        return $this->loan;
    }
}
