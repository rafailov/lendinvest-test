<?php

namespace LendInvest;

use DateTime;

class Investor
{
    /**
     * @var Wallet
     */
    private $wallet;

    /**
     * @var array
     */
    private $investments;

    /**
     * Investor constructor.
     * @param $amount
     */
    public function __construct($amount)
    {
        $this->wallet = new Wallet($amount);
        $this->investments = [];
    }

    /**
     * Calculate total interest for specific month
     *
     * @param DateTime $date
     * @return float
     */
    public function getInterestForMonth(DateTime $date) : float
    {
        $interest = 0;
        /** @var Investment $investment */
        foreach ($this->investments as $investment) {
            $interest += $investment->calculateInterestForMonth($date);
        }

        return $interest;
    }

    /**
     * @param Investment $investment
     * @return Investor
     */
    public function makeInvestment(Investment $investment) : Investor
    {
        $this->pay($investment->getAmount());
        $this->investments[] = $investment;

        return $this;
    }

    /**
     * @param $amount
     * @return $this
     */
    public function fund($amount) : Investor
    {
        $this->wallet->fund($amount);

        return $this;
    }

    /**
     * @param $amount
     * @return $this
     */
    public function pay($amount) : Investor
    {
        $this->wallet->withdraw($amount);

        return $this;
    }

    /**
     * @param $amount
     * @return bool
     */
    public function hasEnoughFunds($amount) : bool
    {
        return $this->wallet->getFunds() >= $amount;
    }
}
