<?php

namespace LendInvest;

use DateTime;
use Exception;

class Loan
{
    /**
     * @var DateTime
     */
    private $startDate;

    /**
     * @var DateTime
     */
    private $endDate;

    /**
     * Loan constructor.
     * @param DateTime $startDate
     * @param DateTime $endDate
     * @throws Exception
     */
    public function __construct(DateTime $startDate, DateTime $endDate)
    {
        if ($startDate > $endDate) {
            throw new Exception('End date must be after start date');
        }

        $this->startDate = $startDate;
        $this->endDate = $endDate;
    }

    /**
     * @return DateTime
     */
    public function getStartDate() : DateTime
    {
        return $this->startDate;
    }

    /**
     * @return DateTime
     */
    public function getEndDate() : DateTime
    {
        return $this->endDate;
    }

    /**
     *
     * @param DateTime $investmentDate
     * @return bool
     */
    public function isOpen(DateTime $investmentDate) : bool
    {
        return ($this->getStartDate() <= $investmentDate && $investmentDate <= $this->getEndDate());
    }
}
